SET ECHO ON;
		SELECT distinct eid
		FROM Empresas esGlobal
		WHERE NOT EXISTS
		(
		    SELECT descricao_textual FROM Licitacoes l
		    WHERE lid = '658960' AND lote = 'LOTE6' AND NOT EXISTS
		    (
			SELECT * FROM Empresas esLocal
			WHERE esLocal.eid = esGlobal.eid
			AND similarityTree(esLocal.descricao_textual, l.descricao_textual,'ontologia.txt', 2) = 1 
		    )
		);
		EXIT;
		