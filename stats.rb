#!/usr/bin/env ruby

require 'set'

dir = ARGV[0]
if not dir then
  STDERR.puts "Usage: stats.rb <result directory>"
  exit
end

def fmt3f(x)
  (x*1000).round.fdiv 1000
end

def parseresult(f)
  v = []
  s = 0
  IO.foreach(f) do |l|
    s = 0 if s == 1 and l.strip.length == 0
    v << l.strip if s == 1
    s = 1 if l.match /^---/
  end
  v
end

testset = Set.new ["BrotoLegal", "CentralFoods", "Concorde", "Dequech", "Gaiteiro", "Hemmer", "Hikari", "Portela", "SLCAlimentos", "Sadia", "Urbano"]
q = {} # query results by edital_lote
`ls #{dir}/result*`.strip.split.each do |f|
  prefix, edital, lote, onto = f.split '_'
  res = parseresult(f).sort
  id = edital+"_"+lote
  (testset & res).each do |e|
    q[e] = Set.new if not q[e]
    q[e] << id
  end
  # print prefix,"\t",onto,"\t",edital,"\t",lote,"\t",res.join(","),"\n"
end

ttp = 0
tfp = 0
tfn = 0
ttn = 0
v_tpr = []
v_tnr = []
v_fpr = []
v_fnr = []
v_ppv = []
v_acc = []

lotes = Set.new
r = {} # real classes
IO.foreach("lotes.txt") do |s|
  edital, lote, count, res = s.split "\t"
  id = edital+"_"+lote
  lotes << id
  res = res.split(/,/).map {|w| w.strip.delete ' '}
  (testset & res).each do |e|
    r[e] = Set.new if not r[e]
    r[e] << id
  end
  # print edital,"\t",lote,"\t",count,"\t",res.join(","),"\n"
end

N = lotes.length
emptyset = Set.new
testset.each do |e|
  x = r[e] || emptyset
  y = q[e] || emptyset
  tp = (x & y).length
  fp = (y - x).length
  fn = (x - y).length
  # fp = (y.map {|w| (x.include? w) ? 0 : 1} .reduce(:+)) || 0
  # fn = (x.map {|w| (y.include? w) ? 0 : 1} .reduce(:+)) || 0
  # tp = (y.map {|w| (x.include? w) ? 1 : 0} .reduce(:+)) || 0
  tn = N - tp - fp - fn
  ttp += tp
  tfp += fp
  ttn += tn
  tfn += fn
  rp = tp + fn
  rn = tn + fp
  pp = tp + fp
  pn = tn + fn
  tpr = tp.fdiv(rp)
  tnr = tn.fdiv(rn)
  fpr = fp.fdiv(rn)
  fnr = fn.fdiv(rp)
  ppv = tp.fdiv(pp)
  acc = (tp+tn).fdiv(N)
  v_tpr << tpr if not tpr.nan?
  v_tnr << tnr if not tnr.nan?
  v_fpr << fpr if not fpr.nan?
  v_fnr << fnr if not fnr.nan?
  v_ppv << ppv if not ppv.nan?
  v_acc << acc if not acc.nan?
  itpr = tpr.nan? ? "-" : fmt3f(tpr)
  itnr = tnr.nan? ? "-" : fmt3f(tnr)
  ifpr = fpr.nan? ? "-" : fmt3f(fpr)
  ifnr = fnr.nan? ? "-" : fmt3f(fnr)
  ippv = ppv.nan? ? "-" : fmt3f(ppv)
  iacc = acc.nan? ? "-" : fmt3f(acc)
  print itpr,"\t",itnr,"\t",ifpr,"\t",ifnr,"\t",ippv,"\t",iacc,"\t:",e,"\n"
end

trp = ttp + tfn
trn = ttn + tfp
tpp = ttp + tfp

qt = []
`grep Finished queries.log`.strip.split("\n").each do |s|
  qt << s.split[2].to_f
end

qt.sort!
v_tpr.sort!
v_tnr.sort!
v_fpr.sort!
v_fnr.sort!
v_ppv.sort!
v_acc.sort!

def stats(v, name)
  n = v.length
  m = v[n/2]
  a = v.reduce(:+).fdiv n
  d = v.map {|x| (x-a)**2}
  s = Math.sqrt(d.reduce(:+).fdiv n)
  m = fmt3f m
  a = fmt3f a
  s = fmt3f s
  print name,"\t",m,"\t",a,"\t",s,"\n"
  min = v.min
  max = v.max
  z = v.map {|x| (x-min)/(max-min)}
  b = 10
  hist = [0]*b
  z.each do |x|
    bucket = (x * (b - 0.001)).floor
    hist[bucket] += 1
  end
  hist.map {|x| (20*x/n).to_i}
end

def print_hs(hs)
  max = hs.map {|h|h.max} .max
  (0..max).each do |y|
    hs.each do |h|
      b = h.length
      (0..b-1).each do |x|
        print (h[x]>(max-y) ? '█' : ' ')
      end
      print "\t"
    end
    print "\n"
  end
  hs.each do |h|
    (h.length).times {print "▔"}
    print "\t"
  end
  print "\n"
  # hist.each do |x|
  #   print x.fdiv(v.length),"\n"
  # end
end

puts "\t\tmedian\tavg\tstd.dev."
ht = stats qt,"Runtime    "
ha = stats v_acc,"Accuracy "
hp = stats v_ppv,"Precision"
hr = stats v_tpr,"Recall   "
hs = stats v_tnr,"Specif.  "
hf = stats v_fpr,"Fallout  "
hm = stats v_fnr,"Miss-Rate"
print "\n Runtime\tAccuracy\tPrecision\t Recall \tSpecificity\tFall-out\tMiss-Rate"
print_hs [ht,ha,hp,hr,hs,hf,hm]

# s = 0
# `ls #{dir}/* | grep -o '_[^_]*$' | sort | uniq -c | awk '{print $1}'`.split.each do |w|
# 	s += w.to_i
# end
# print (s*100/(34*7)),"% complete.\n"

def graph(spr, file)
  gpr = {}
  spr.each do |r,p|
    gpr[r] = [] if not gpr[r]
    gpr[r] << p
  end
  File.open(file,"w") do |f|
    gpr.sort.map do |r,p|
      p = p.sort.map {|x| x.fdiv(100)}
      m = p.length/2
      n = p.length/5
      a,b,c,d = p[0],p[m-n],p[m+n],p[-1]
      # m = p.reduce(:+).fdiv p.length
      m = p[m]
      f.print [r.fdiv(100),a,b,m,c,d,"\n"].join("\t")
    end
    gpr.map {|r,p| [r,(p.reduce(:+).fdiv(p.length).to_i),p.min,p.max]} .sort.each do |r,p,min,max|
      print r," "
      (min/2).times { print "k" }
      ((p-min)/2).times { print "+" }
      ((max-p)/2).times { print "-" }
      print "\n"
    end
  end
end
# puts "\nPrecision × Recall"
# graph spr,"graph_PR.txt"
# puts "\nAccuracy × Recall"
# graph sar,"graph_AR.txt"
q = fmt3f (trp).fdiv(trp+trn)
a = fmt3f (ttp+ttn).fdiv(trp+trn)
p = fmt3f ttp.fdiv(tpp)
r = fmt3f ttp.fdiv(trp)
s = fmt3f ttn.fdiv(trn)
o = fmt3f tfp.fdiv(trn)
m = fmt3f tfn.fdiv(trp)
f = fmt3f 2*(p*r)/(p+r)
g = fmt3f Math.sqrt(p*r)
print "Prevalence \t",q,"\n"
print "Accuracy   \t",a,"\n"
print "Precision  \t",p,"\n"
print "Recall     \t",r,"\n"
print "Specificity\t",s,"\n"
print "Fallout    \t",o,"\n"
print "Miss-Rate  \t",m,"\n"
print "F-Measure  \t",f,"\n"
print "G-Measure  \t",g,"\n"
