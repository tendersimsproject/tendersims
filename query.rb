#!/usr/bin/env ruby

edital, lote, onto, k = ARGV
user = 'myuser'
pass = 'mypass'

if edital and lote and onto then
	File.open("queries.log", "a") do |log|
		query = %(SET ECHO ON;
		SELECT distinct eid
		FROM Empresas esGlobal
		WHERE NOT EXISTS
		(
		    SELECT descricao_textual FROM Licitacoes l
		    WHERE lid = '#{edital}' AND lote = '#{lote}' AND NOT EXISTS
		    (
			SELECT * FROM Empresas esLocal
			WHERE esLocal.eid = esGlobal.eid
			AND similarityTree(esLocal.descricao_textual, l.descricao_textual,'#{onto}.txt', #{k}) = 1 
		    )
		);
		EXIT;
		)
		log.write "Running query for:\n#{edital}\t#{lote}\t#{onto}\n"
		File.open("q.sql","w") do |f|
			f.write query
		end
		if `ls result_#{edital}_#{lote}_#{onto}.txt 2>/dev/null`.strip.length == 0 then
			t1 = Time.now
			`sqlplus #{user}/#{pass} @q.sql >result_#{edital}_#{lote}_#{onto}.txt`
			t2 = Time.now
			log.write "Finished in #{(t2-t1)*1.0} s\n"
		else
			log.write "Aborting query, result file already exists.\n"
		end
	end
end
