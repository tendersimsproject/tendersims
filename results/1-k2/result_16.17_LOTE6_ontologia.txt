
SQL*Plus: Release 12.1.0.2.0 Production on Wed Oct 18 20:56:17 2017

Copyright (c) 1982, 2014, Oracle.  All rights reserved.

Last Successful login time: Wed Oct 18 2017 20:46:10 -02:00

Connected to:
Oracle Database 12c Enterprise Edition Release 12.1.0.2.0 - 64bit Production
With the Partitioning, OLAP, Advanced Analytics and Real Application Testing options

SQL> 		     SELECT distinct eid
  2  		     FROM Empresas esGlobal
  3  		     WHERE NOT EXISTS
  4  		     (
  5  			 SELECT descricao_textual FROM Licitacoes l
  6  			 WHERE lid = '16.17' AND lote = 'LOTE6' AND NOT EXISTS
  7  			 (
  8  			     SELECT * FROM Empresas esLocal
  9  			     WHERE esLocal.eid = esGlobal.eid
 10  			     AND similarityTree(esLocal.descricao_textual, l.descricao_textual,'ontologia.txt', 2) = 1
 11  			 )
 12  		     );

EID
--------------------------------------------------------------------------------
Urbano
Concorde
Gaiteiro
Tordilho
Dequech
SLCAlimentos

6 rows selected.

SQL> 		     EXIT;
Disconnected from Oracle Database 12c Enterprise Edition Release 12.1.0.2.0 - 64bit Production
With the Partitioning, OLAP, Advanced Analytics and Real Application Testing options
