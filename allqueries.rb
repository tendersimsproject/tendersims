#!/usr/bin/env ruby

lotes = []
k = 2

IO.foreach("lotes.txt") do |s|
	lotes << s.strip.split
end

File.open("queries.log", "a") do |log|
	log.write "\nStarting session...\n"
	lotes.each do |edital,lote|
		["inputtree","ontologia"].each do |onto|
			`./query.rb #{edital} #{lote} #{onto} #{k}`
		end
	end
end
