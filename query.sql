SELECT distinct eid
FROM Empresas esGlobal
WHERE NOT EXISTS
(
    SELECT descricao_textual FROM Licitacoes l
    WHERE lid = '118' AND lote = 'LOTE7' AND NOT EXISTS
    (
        SELECT * FROM Empresas esLocal
        WHERE esLocal.eid = esGlobal.eid
        AND similarityTree(esLocal.descricao_textual, l.descricao_textual,'inputtree.txt',2) = 1 
    )
);
