// ==UserScript==
// @name         Crawler ConcordePR
// @namespace    http://hidden/
// @version      0.1
// @author       hidden
// @match        http://www.concordepr.com.br/shop/*
// @grant        none
// ==/UserScript==

(function() {
    var url = document.baseURI.match(/^http.*shop\/(.*).html(.*)/);
    var name = url[1].replace('/','-');
    if (url[2]) name += url[2].replace('?p=','');
    var items = document.getElementsByClassName("product-image");
    var data = [];
    for (var i in items) {
        var e = items[i];
        if (e.tagName == "A") {
            data.push(['"',e.title,'","',e.children[0].src,'"'].join(""));
        }
    }
    var a = document.createElement("a");
    a.href = "data:text/plain;base64," + btoa(data.join("\n"));
    a.download = "concordepr-"+name+".txt";
    a.innerText = "Download (CSV)";
    document.body.append(a);
    a.click();
})();
