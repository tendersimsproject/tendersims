// ==UserScript==
// @name         Crawler Dequech
// @namespace    http://hidden
// @version      0.1
// @author       hidden
// @match        http://www.ddequech.com.br/produtos/pauta-completa/*
// @grant        none
// ==/UserScript==

(function() {
    function b64(str) {
        return btoa(encodeURIComponent(str).replace(
            /%([0-9A-F]{2})/g,
            function toSolidBytes(match, p1) {
                return String.fromCharCode('0x' + p1);
            }));
    }
    var url = document.baseURI.match(/([/]*)$/);
    var name = url[1].replace('2?page=','');
    var data = [];
    $('.product-list li').each((i,e) => {
        var img = $(e).find("img").attr("src").replace(/^\//, "http://www.ddequech.com.br/");
        var txt = $(e).find("h4").html();
        data.push(['"',txt,'","',img,'"'].join(""));
    });
    var a = document.createElement("a");
    a.href = "data:text/plain;base64," + btoa(data.join("\n"));
    a.download = "dequech-"+name+".txt";
    a.innerText = "Download (CSV)";
    document.body.append(a);
    a.click();
})();
