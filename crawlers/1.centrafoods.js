// ==UserScript==
// @name         Crawler Central Foods
// @namespace    http://hidden/
// @version      0.1
// @author       hidden
// @match        http://www.centralfoods.com.br/produtos_itens/*
// @grant        none
// ==/UserScript==

(function() {
    function b64(str) {
        return btoa(encodeURIComponent(str).replace(
            /%([0-9A-F]{2})/g,
            function toSolidBytes(match, p1) {
                return String.fromCharCode('0x' + p1);
            }));
    }
    var url = document.baseURI.match(/produtos_itens\/(.*)\/$/);
    var name = url[1].replace('/','-');
    var items = document.getElementsByClassName("entry-content-wrapper")[0].children;
    var data = [];
    for (var i=0; i<items.length; i++) {
        var e = items[i];
        if (e.className.match("flex_column")) {
            var img = e.children[0].firstChild.lastChild.src;
            var txt = e.children[2].innerText.replace(/\s+/g," ");
            data.push(['"',txt,'","',img,'"'].join(""));
        }
    }
    var a = document.createElement("a");
    a.href = "data:text/plain;base64," + b64(data.join("\n"));
    a.download = "centralfoods-"+name+".txt";
    a.innerText = "Download (CSV)";
    document.body.append(a);
    a.click();
})();
