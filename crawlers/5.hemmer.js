// ==UserScript==
// @name         Crawler Hemmer
// @namespace    http://hidden/
// @version      0.1
// @author       hidden
// @match        http://www.hemmer.com.br/produtos?page=*
// @grant        none
// ==/UserScript==

(function() {
    var name = window.location.toString().match(/[0-9]*$/)[0];
    var data = [];
    $('.produto').each(function(){
        data.push(['"',$(this).find('.titulo').text().trim(),'","',document.baseURI+$(this).find('.image-produto img').attr('src'),'"'].join(""));
    });
    var a = document.createElement("a");
    a.href = "data:text/plain;base64," + btoa(data.join("\n"));
    a.download = "hemmer-"+name+".txt";
    a.innerText = "Download (CSV)";
    document.body.append(a);
    a.click();
})();
