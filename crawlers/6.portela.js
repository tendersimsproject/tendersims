// ==UserScript==
// @name         Crawler Portela
// @namespace    http://hidden
// @version      0.1
// @author       hidden
// @match        http://www.armazemportela.com.br/*
// @grant        none
// ==/UserScript==

(function() {
    function b64(str) {
        // first we use encodeURIComponent to get percent-encoded UTF-8,
        // then we convert the percent encodings into raw bytes which
        // can be fed into btoa.
        return btoa(encodeURIComponent(str).replace(
            /%([0-9A-F]{2})/g,
            function toSolidBytes(match, p1) {
                return String.fromCharCode('0x' + p1);
            }));
    }
    var url = document.baseURI.match(/([/]*)$/);
    var name = url[1].replace('2?page=','');
    var data = [];
    var items = document.getElementsByClassName("product-frame");
    for (var i=0; i<items.length; i++) {
        var e = items[i];
        var img = e.children[0].children[0].children[0].children[0].src.replace(/^\//, "http://www.armazemportela.com.br/");
        var txt = e.children[1].innerText.replace(/\)[^)]*$/,')');
        var suffix = e.children[1].children[0].children[0].children[0].href.match(/(\d+[km]?[lg])\/$/);
        if (suffix) txt += " " + suffix[1];
        data.push(['"',txt,'","',img,'"'].join(""));
    }
    var a = document.createElement("a");
    a.href = "data:text/plain;base64," + btoa(data.join("\n"));
    a.download = "portela-"+name+".txt";
    a.innerText = "Download (CSV)";
    document.body.append(a);
    a.click();
})();
